<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>EyeKiller Test</title>

	<link rel="stylesheet" href="css/product-lists/product-list-basic.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

</head>
<body>


	<label>Filter:</label> 
	<input type="text" class="search-form" />

	<a class="category-filter" href="#" data-category="all">All</a> |
	<a class="category-filter" href="#" data-category="classic">Classic</a> |
	<a class="category-filter" href="#" data-category="contemporary">Contemporary</a> |
	<a class="category-filter" href="#" data-category="family">Family</a> |
	<a class="category-filter" href="#" data-category="mainstream">Mainstream</a>

    <ul class="product-list-basic">

	    @foreach ($events AS $event) 

	        <li class="event" data-classic="{{ $event->attribute_MACClassic }}" data-contemporary="{{ $event->attribute_MACContemporary }}" data-family="{{ $event->attribute_MACFamily }}" data-mainstream="{{ $event->attribute_MACMainstream }}">

	            <a href="#" class="product-photo">
	                <img src="images/event.jpg" height="130" alt="{{ $event->name }}" />
	            </a>

	            <h2><a href="#">{{ $event->name }}</a></h2>

	            <p class="hidden-description">

	            	{{ $event->name . ' ' . $event->description }}

	            </p>

	            <span>{{ $event->instanceDates }}</span>

	            <div class="product-rating">
	                <div>
	                        <span class="product-stars" style="width: 60px" >
	                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
	                        </span>
	                </div>

	                <span><a href="#"><? echo rand(10, 100) . ' reviews' ?></a></span>
	            </div>

	            <p class="product-description">{{ substr($event->description, 0, 200) . '...' }}</p>

	            <button>Book Now!</button>
	            <p class="product-price"><? echo '£' . rand(10, 100) . '.00' ?></p>

	        </li>

		@endforeach

    </ul>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/scripts.js"></script>
   

</body>

</html>
