<?php

class EventController extends BaseController {

	public function index()
	{

		$api_login_name     = 'TestLogin';
		$api_secret_key     = 'XXXXXXXX';
		
		//$Body             = '';
		//$BodyStringToSign = base64_encode( md5( utf8_encode( $Body ) ) );
		$StringToSign       = 'HTTP-Method' . "\n" . 'HTTP-Uri' . "\n" . 'HTTP-Date';
		$Signature          = base64_encode( hash_hmac ('sha1' , utf8_encode( $StringToSign ) , $api_secret_key) );
		$Authorization      = 'SpektrixAPI3 ' . $api_login_name . ':' . $Signature;
		
		$todays_date        = date("d/m/Y");

		$client = new GuzzleHttp\Client();
		$res = $client->request('GET', "https://system.spektrix.com/themac/api/v3/events?instanceStart_from=$todays_date", [
			'Authorization' => $Authorization
		]);

		$response = $res->getBody();
		$events   = json_decode($response);

		return View::make('events.index')->with(compact('events'));

	}

}
