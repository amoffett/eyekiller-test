// Extend jquery giving a case insensitive method of 'contains'
$.extend($.expr[":"], {
	"containsIN": function(elem, i, match, array) {
		return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
	}
});

// Keyword searching
$('input.search-form').keyup(function() {

	search_criteria = $(this).val();

	// If text box is empty, show everything
	if (search_criteria == '') {

		$('li.event').show();
		return false;

	}

	// Hide everything then show only what is found
	$('li.event').hide();

	$('li p.hidden-description:containsIN('+ search_criteria +')').parent('li').show();

});

$('a.category-filter').click(function() {

	// Hide everything then show only what is found
	$('li.event').hide();

	selected_category = $(this).attr('data-category')

	if (selected_category == 'all') {
		$('li.event').show();
	}
	else if (selected_category == 'classic') {
		$('li.event[data-classic="1"]').show();
	}
	else if (selected_category == 'contemporary') {
		$('li.event[data-contemporary="1"]').show();
	}
	else if (selected_category == 'family') {
		$('li.event[data-family="1"]').show();
	}
	else if (selected_category == 'mainstream') {
		$('li.event[data-mainstream="1"]').show();
	}

	return false;

});
